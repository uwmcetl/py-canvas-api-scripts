# README #

## About py-canvas-api-scripts ##

* A collection of one or more scripts designed to process simple, arbitrary API calls one-at-a-time or in batch mode.
* Latest version: 1.1 published January 18, 2022.
* Published under the MIT License

## Setup ##

* Requires at least Python 3.7
* (Optional) Set-up a Python environemnt
* Install pandas, and requests via PIP
* Download a copy of the repository from the Downloads page, Tags tab

## How to Use ##

### canvas-api-request.py ###

    Usage: canvas-api-request.py [-h] [-d DATA] [-m METHOD] [-p PATHVARS] [-q QUERYPARAMS] [-b BATCH] [-t TOKEN] endpoint

Strictly speaking, there is one parameter that is required

    endpoint              Full API endpoint to call: httsp://some.site/api/:index/get

There are other options which may or may not need to be used at the same time.

    -m METHOD, --method METHOD
                            Type of call to make: GET, POST, PUT, DELETE

The default is GET

    -t TOKEN, --token TOKEN
                            The bearer token to send via header to make the call

Almost all API calls require user authentication

    -p PATHVARS, --pathvars PATHVARS
                            JSON String of path parameters (:name) -- {"key": "value"}

Anything in the endpoint leading with a colon will be replaced with whatever you provide 
here in JSON format. Depending on the operating system, the doublequotes are required, 
and may need to be escaped with \ (e.g. {\"key\": \"value\"} for PowerShell). For path
variables, do not include the color (:).

Python cares about spacing.

    -q QUERYPARAMS, --queryparams QUERYPARAMS
                            JSON string of query parameter values -- {"key": "value"}

Like with PATHVARS, using double quotes, obeying spacing rules, and escaping double 
quotes with \ are likely.

    -d DATA, --data DATA    
                            If METHOD is POST or PUT, JSON string of data -- {"key": "value"}

NOT FULLY IMPLEMENTED, DO NOT USE

    -b BATCH, --batch BATCH
                            Load data from CSV file, with columns either path parameters (:name) or 
                            query parameters (name).

Specify the path to the CSV file you wish to read. The CSV file's first row should contain
path variables (starting with ":") or query vars which would go after the ? in the endpoint
call. Each following row should provide the information for one API call. If you specify
BATCH, QUERYPARAMS and PATHVARS will be ignored.

    -o OUTPUT, --output OUTPUT
                            The desired CSV filename; incldue the extension.

The desired name of the file to save output to. Include the .csv extension or your computer may
not understand what to do with the file.

    -r, --replicate         Add path vars and query vars to the CSV output.

Add any values in the batch file, or provided in PATHVARS or QUERYVARS to the start of the row output. If
-r is used with -b, the values from the input CSV will be prepended to the row output. Otherwise, any 
values specified with -p and/or -q will be added.

    -h, --help              show this help message and exit

A handy-if-brief rehash of this information

### Examples ###

    python3 canvas-api-request.py -m GET -t BEARER_TOKEN https://domain.instructure.com/api/v1/accounts

Get all accounts available to the user whose bearer token was provided. The response is returned in CSV.

    python3 canvas-api-request.py -m GET -p {":course_id": "103211"} \
        -t BEARER_TOKEN https://domain.instructure.com/api/v1/courses/:course_id/enrollments

Get enrollments for the course identified with Canvas ID number 103211.

    python3 canvas-api-request.py -m GET -p {":course_id": "103211"} \
        -t BEARER_TOKEN https://domain.instructure.com/api/v1/courses/:course_id/enrollments

Get enrollments for the course identified with Canvas ID number 103211. The results of all pages will be saved to a CSV file.

    python3 canvas-api-request.py -m GET -p {":course_id": "103211"} \
        -q {"type[]": "TeacherEnrollment"} -t BEARER_TOKEN \
        https://domain.instructure.com/api/v1/courses/:course_id/enrollments

Get enrollments for the course identified with Canvas ID number 103211. Results will be limitied to Teacher 
Enrollments. The results of all pages will be saved to a CSV file. 

    python3 canvas-api-request.py -m GET -b courses.csv -t BEARER_TOKEN \
        https://domain.instructure.com/api/v1/courses/:course_id/enrollments


    courses.csv:
    :courseid,type[]
    103211,TeacherEnrollment
    98422,StudentEnrollment

Get enrollments based on data in a CSV file named courses.csv. Enrollments for course ID 103211
will be restricted to TeacherEnrollment. Enrollments for course 98422 will be restricted to 
StudentEnrollment. All paged results from both calls will be saved to the same CSV file.

    python3 canvas-api-request.py -m GET -r -b users.csv -o details.csv -t BEARER_TOKEN \
        https://uwmil.instructure.com/api/v1/users/:id

    users.csv:
    :id,include[]
    15742,last_login
    26436,last_login

Get user account details for users in users.csv, and save the results (with the :id and include[] 
values) to a file named details.csv.

## For Help##

* Email: delgadcd@uwm.edu