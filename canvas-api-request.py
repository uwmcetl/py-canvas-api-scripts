#!/usr/bin/env python

"""
A command line utility to execute an API call, which can either output the 
results to the screen (POST, PUT, DELETE), or a CSV file (for GET calls only).
This code is tailored to work best with Canvas LMS, and expects a bearer token
to be used
"""

from urllib.parse import urlencode
from datetime import datetime
import argparse
import json
import csv
import requests
import pandas as pd
import time

__author__ = "David Delgado <delgadcd@uwm.edu>"
__version__ = "1.3"
__license__ = "MIT"


#
# Constants
#

API_METHOD_GET = 'GET'
API_METHOD_PUT = 'PUT'
API_METHOD_POST = 'POST'
API_METHOD_DELETE = 'DELETE'
DEFAULT_API_METHOD = API_METHOD_GET
CALL_DELAY = 0.15

# Message strings
MSG_SUCCEESS = '    Success'
MSG_ERR = '    Failed With Code: '
MSG_FILEWRITE = 'Writing results to CSV'
MSG_FINISH = 'Done!'
HELP_MSG_DESC = ('Execute a REST API call and save the GET response to'
                 'CSV, or print the response for all other methods.')
HELP_MSG_METHOD = 'Type of call to make: GET, POST, PUT, DELETE'
HELP_MSG_ENDPOINT = ('Full API endpoint to call: '
                     'httsp://some.site/api/:index/get')
HELP_MSG_TOKEN = 'The bearer token to send via header to make the call'
HELP_MSG_PATHVARS = ('JSON String of path parameters (:name) -- '
                     '{\"key\": \"value\"}')
HELP_MSG_QUERYPAR = ('JSON string of query parameter values -- {\"key\": '
                     '\"value\"}')
HELP_MSG_DATA = ('If METHOD is POST or PUT, JSON string of data -- '
                 '{\"key\": \"value\"}')
HELP_MGS_OUTPUTFILE = 'The desired CSV filename; incldue the extension.'
HELP_MSG_REPLICATE = 'Add path vars and query vars to the CSV output.'
HELP_MSG_BATCH = ('Load data from CSV file, with columns either path'
                  ' parameters (:name) or query parameters (name).')


#
# Functions
#


def stage_arguments():
    """Configure arguments for the script"""

    handler = argparse.ArgumentParser(description=HELP_MSG_DESC)
    handler.add_argument('endpoint', type=str, help=HELP_MSG_ENDPOINT)
    handler.add_argument('-d', '--data', type=str, help=HELP_MSG_DATA)
    handler.add_argument('-m', '--method', type=str, help=HELP_MSG_METHOD)
    handler.add_argument('-p', '--pathvars', type=str,
                         help=HELP_MSG_PATHVARS)
    handler.add_argument('-q', '--queryparams', type=str,
                         help=HELP_MSG_QUERYPAR)
    handler.add_argument('-b', '--batch', type=str, help=HELP_MSG_BATCH)
    handler.add_argument('-o', '--output', type=str, help=HELP_MGS_OUTPUTFILE)
    handler.add_argument('-r', '--replicate', help=HELP_MSG_REPLICATE, action='store_true')
    handler.add_argument('-t', '--token', type=str, help=HELP_MSG_TOKEN)

    return handler.parse_args()


def execute_call(endpoint, method, call_headers, pageidx=1, call_data=None):
    """ Execute the API call and return the response """

    response = {}

    print('Page ' + str(pageidx) + ': ' + endpoint)
    if method == API_METHOD_GET:
        response = requests.get(endpoint, headers=call_headers)
    elif method == API_METHOD_POST:
        response = requests.post(endpoint, data=call_data,
                                 headers=call_headers)
    elif method == API_METHOD_PUT:
        response = requests.put(endpoint, data=call_data,
                                headers=call_headers)
    elif method == API_METHOD_DELETE:
        response = requests.delete(endpoint, headers=call_headers)

    return response


def parse_link_header(link_header):
    """Parse a link header"""

    response = {
        'current': None,
        'next': None,
        'previous': None,
        'first': None,
        'last': None
    }

    header_links = requests.utils.parse_header_links(link_header)
    for link in header_links:
        response[link['rel']] = link['url']

    return response


def process_call_success(response_text, call_data=None):
    """Append call data to existing data"""

    response = call_data

    print(MSG_SUCCEESS)

    parsed_json = json.loads(response_text)
    if type(parsed_json) is dict:
        parsed_json = [parsed_json]
    
    if response is None:
        response = parsed_json
    else:
        response = response + parsed_json

    return response


def set_api_call(args):
    """Turn arguments into an API Call dict"""

    response = {
        'endpoint':     '',
        'method':       DEFAULT_API_METHOD,
        'header':       {},
        'path_vars':    {},
        'query_params': {},
        'data':         None,
        'url_params':   '',
        'batch':        None,
        'output':       None,
        'replicate':    False
    }

    if args.endpoint is None:
        return None
    else:
        response['endpoint'] = args.endpoint

    if args.method is not None:
        response['method'] = args.method

    if args.token is not None:
        response['header'] = {'Authorization': 'Bearer ' + args.token}

    if args.pathvars is not None:
        response['path_vars'] = json.loads(args.pathvars.replace("'", '"'))       
        response['endpoint'] = insert_pathvars(
            response['path_vars'], response['endpoint'])

    if args.queryparams is not None:
        response['query_params'] = json.loads(args.queryparams)
        response['url_params'] = '?' + urlencode(response['query_params'])

    if args.data is not None and (response['method'] == API_METHOD_PUT or response['method'] == API_METHOD_POST):
        response['data'] = json.loads(args.data)

    if args.batch is not None:
        response['batch'] = args.batch

    if args.output is not None:
        response['output'] = args.output

    if args.replicate is not None:
        response['replicate'] = args.replicate

    return response


def insert_pathvars(values, endpoint):
    """Replace :value in an endpoint with elements in values"""

    response = endpoint

    for key in values:
        path_var = ':' + key
        key_value = str(values[key])
        response = response.replace(path_var, key_value)

    return response


def proecess_get_call(original_call_obj, rows=None):
    """Process a call specific to GET which might need pagination"""

    response = None

    if rows is not None:
        call_objs = parse_batch_rows(rows, original_call_obj['endpoint'])
    else:
        call_objs = [original_call_obj]

    record_count = len(call_objs)
    current_record = 1

    for call_obj in call_objs:

        next_page = 1
        page_counter = 1

        print('Processing call ' + str(current_record) + ' of ' +
              str(record_count) + '. (' + str(round((current_record / record_count) * 100, 2)) + '%)')

        while next_page:

            api_response = execute_call(call_obj['endpoint']
                                        + call_obj['url_params'], original_call_obj['method'],
                                        original_call_obj['header'], page_counter, original_call_obj['data'])
            if api_response.status_code == 200:
                response = process_call_success(api_response.text, response)
                if original_call_obj['replicate']:
                    response = append_vars(response, call_obj)
            else:
                print(MSG_ERR + str(api_response.status_code))
                next_page = 0
                break

            response_headers = lowercase_keys(api_response.headers)

            if 'link' not in response_headers:
                next_page = 0
                break

            if 'rel="next"' not in response_headers['link']:
                next_page = 0
            else:
                relationships = parse_link_header(response_headers['link'])
                if (relationships['current'] == relationships['next']):
                    next_page = 0
                    break
                else:
                    next_url_parts = relationships['next'].split('?')
                    call_obj['url_params'] = '?' + next_url_parts[1]

            page_counter = page_counter + 1

            time.sleep(CALL_DELAY)

        current_record += 1

    # Escape strings so CSV output makes sense
    if response is not None:
        for row in response:
            for key in row:
                row[key] = str(row[key]).encode('unicode_escape').decode("utf-8")
    
    return response


def append_vars(original_obj, new_obj):

    response = None

    new_record_data = dict()
    new_record_data.update(new_obj['path_vars'])
    new_record_data.update(new_obj['query_params'])

    if type(original_obj) is dict:
        response = dict()
        response.update(new_record_data)
        response.update(original_obj)

    else:
        response = []
        for record in original_obj:
            new_record = dict()
            new_record.update(new_record_data)
            new_record.update(record)
            response.append(new_record)

    return response


def lowercase_keys(source_dict):

    response = {}

    for key in source_dict:
        new_key = str(key).lower()
        response[new_key] = source_dict[key]

    return response


def parse_batch_rows(rows, endpoint):
    """Convert CSV rows (in list form) into an array of call objects"""

    response = []

    for row in rows:

        new_call_obj = {
            'path_vars': {},
            'query_params': {},
            'endpoint': endpoint
        }

        for key in row:

            if key.startswith(':'):
                new_key = key[1:]
                new_call_obj['path_vars'][new_key] = row[key]
            else:
                new_call_obj['query_params'][key] = row[key]

        new_call_obj['url_params'] = '?' + \
            urlencode(new_call_obj['query_params'])
        new_call_obj['endpoint'] = insert_pathvars(
            new_call_obj['path_vars'], new_call_obj['endpoint'])
        response.append(new_call_obj)

    return response


def process_call(original_call_obj, rows=None):
    """Execute a non-paganated call"""

    response = []

    if rows is not None:
        call_objs = parse_batch_rows(rows, original_call_obj['endpoint'])
    else:
        call_objs = [original_call_obj]

    for call_obj in call_objs:

        api_response = execute_call(call_obj['endpoint'] + call_obj['url_params'],
                                    original_call_obj['method'], original_call_obj['header'],
                                    call_data=original_call_obj['data'])

        if api_response.status_code == 200:
            response.append(process_call_success(api_response.text, None))
        else:
            print(MSG_ERR + str(api_response.status_code) + ' | ' + api_response.reason)
            print(json.dumps(dict(api_response.headers), indent=4))

        time.sleep(CALL_DELAY)

    return response


def write_csv(file_name, data):
    """Write data to a CSV file"""

    print(MSG_FILEWRITE)
    data_frame = pd.DataFrame(data)
    data_frame.to_csv(file_name, index=False)


def get_file_name():
    """Create a CSV filename with date and timestamp"""

    today = datetime.now()
    file_name = today.strftime('%Y-%m-%d_%H-%M-%S-%f') + '.csv'

    return file_name


def parse_batch_file(file_path):
    """Read a CSV file and convert it to an array of objects"""

    response = []

    file_handler = open(file_path)
    file_parser = csv.DictReader(file_handler)

    for row in file_parser:
        response.append(row)

    return response


#
# Main function and Module
#


def main():
    """Interprets arguments, executes call, then present output"""

    arguments = stage_arguments()
    api_call = set_api_call(arguments)

    if api_call is None:
        quit()

    if api_call['batch'] is not None:
        batch_records = parse_batch_file(api_call['batch'])
    else:
        batch_records = None

    if api_call['method'] == API_METHOD_GET:
        response_data = proecess_get_call(api_call, rows=batch_records)
        if response_data is not None:
            if api_call['output'] is not None:
                file_name = api_call['output']
            else:
                file_name = get_file_name()
            write_csv(file_name, response_data)

    else:
        response_data = process_call(api_call, rows=batch_records)
        if response_data is not None:
            print(json.dumps(response_data, indent=4))
        else:
            quit()

    print(MSG_FINISH)


if __name__ == '__main__':
    """Run when started from the command line"""

    main()
